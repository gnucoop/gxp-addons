# GXP add-ons #

Set of add-ons for [GXP components](http://gxp.opengeo.org/master/doc/), high-level components GeoExt based applications, part of the OpenGeo Client SDK API.

## Plugins ##

### MenuFill ###

Plugin for adding a menu filler in a toolbar or a context menu

### MenuSeparator ###

Plugin for adding a menu separator in a toolbar or a context menu

### LayerOpacitySlider ###

Plugin for setting layer opacity by a slider added to context menu

## Authors ##

Marco Marche ([marco.marche@gnucoop.com](mailto:marco.marche@gnucoop.com))

## License ##

[GPL Licence](http://www.gnu.org/licenses/gpl.html)


## gnucoop ##

[![](http://www.gnucoop.com/wp-content/uploads/2012/07/gnulogo-it2.png)](http://www.gnucoop.com)