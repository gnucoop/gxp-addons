/**
 * Copyright (c) 2013 gnucoop
 * 
 * Published under the GPL license.
 * See http://www.gnu.org/licenses/gpl.html for the full text
 * of the license.
 */

/** api: (define)
 *  module = gc.plugins
 *  class = MenuSeparator
 */

/** api: (extends)
 *  plugins/Tool.js
 */
Ext.namespace("gxp.plugins");
Ext.namespace("gc.plugins");

/** api: constructor
 *  .. class:: MenuSeparator(config)
 *
 *    Plugin for adding a menu separator in a toolbar or a context menu
 */
gc.plugins.MenuSeparator = Ext.extend(gxp.plugins.Tool, {
    
    /** api: ptype = gc_menuseparator */
    ptype: "gc_menuseparator",
    
    actionTarget: null,
    
    addOutput: function(config) {
        if(this.getContainer(this.outputTarget) instanceof Ext.Toolbar)
            return gc.plugins.MenuSeparator.superclass.addOutput.call(this, Ext.apply({
                xtype: "tbseparator"
            }, this));
        else if(this.getContainer(this.outputTarget) instanceof Ext.menu.Menu)
            return gc.plugins.MenuSeparator.superclass.addOutput.call(this, Ext.apply({
                xtype: "menuseparator"
            }, this));
    }
        
});

Ext.preg(gc.plugins.MenuSeparator.prototype.ptype, gc.plugins.MenuSeparator);
