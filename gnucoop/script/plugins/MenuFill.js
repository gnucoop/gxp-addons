/**
 * Copyright (c) 2013 gnucoop
 * 
 * Published under the GPL license.
 * See http://www.gnu.org/licenses/gpl.html for the full text
 * of the license.
 */

/** api: (define)
 *  module = gc.plugins
 *  class = MenuFill
 */

/** api: (extends)
 *  plugins/Tool.js
 */
Ext.namespace("gc.plugins");

/** api: constructor
 *  .. class:: MenuFill(config)
 *
 *    Plugin for adding a menu filler in a toolbar or a context menu
 */
gc.plugins.MenuFill = Ext.extend(gxp.plugins.Tool, {
    
    /** api: ptype = gc_menufill */
    ptype: "gc_menufill",
    
    actionTarget: null,

    addOutput: function(config) {
        return gc.plugins.MenuFill.superclass.addOutput.call(this, Ext.apply({
            xtype: "tbfill"
        }, this));
    }
        
});

Ext.preg(gc.plugins.MenuFill.prototype.ptype, gc.plugins.MenuFill);
