/**
 * Copyright (c) 2013 gnucoop
 * 
 * Published under the GPL license.
 * See http://www.gnu.org/licenses/gpl.html for the full text
 * of the license.
 */

/** api: (define)
 *  module = gc.plugins
 *  class = LayerOpacitySlider
 */

/** api: (extends)
 *  plugins/Tool.js
 */
Ext.namespace("gc.plugins");

/** api: constructor
 *  .. class:: LayerOpacitySlider(config)
 *
 *    Plugin for setting layer opacity by a slider added to context menu
 */
gc.plugins.LayerOpacitySlider = Ext.extend(gxp.plugins.Tool, {
    
    /** api: ptype = gc_layeropacityslider */
    ptype: "gc_layeropacityslider",
    
    labelText: "Opacity",
    
    addOutput: function(config) {
        var selectedLayer;
        
        var opacitySlider = gc.plugins.LayerOpacitySlider.superclass.addOutput.call(this, Ext.apply({
            xtype: 'slider',
            autoWidth: true,
            value: 100,
            increment: 5,
            minValue: 5,
            maxValue: 100,
            listeners: {
                change: function(slider, value) {
                    var record = selectedLayer;
                    if(record) {
                        var layer = this.map.getLayer(record.id);
                        if(layer) {
                            layer.setOpacity(value / 100);
                        }
                    }
                }
            },
            map: this.target.mapPanel.map,
            defaults: {cls: 'gxp-legend-item'}
        }, config));
        
        this.target.on("layerselectionchange", function(record) {
            selectedLayer = record;
            if(record) {
                var layer = this.target.mapPanel.map.getLayer(record.id);
                if(layer) {
                    opacitySlider.setValue(layer.opacity * 100,false);
                }
            }
        }, this);
    }
        
});

Ext.preg(gc.plugins.LayerOpacitySlider.prototype.ptype, gc.plugins.LayerOpacitySlider);
